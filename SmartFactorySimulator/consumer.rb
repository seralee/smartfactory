class Consumer
  def initialize consumed_power, pipe_loss
    @consumed_power = consumed_power
    @pipe_loss = pipe_loss
  end

  def set cvt
    @converter = cvt
  end

  def total_consumed_power
    return @consumed_power + @pipe_loss
  end
  def needed_power
    return [@pipe_loss , @consumed_power].map{|x| BigDecimal(x,3)}.reduce(:+).to_f
  end
  def pipe_loss
    @pipe_loss
  end
end
