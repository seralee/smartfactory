require "bigdecimal"
class Distributor
  def initialize consumed_power 
    @csms = []
    @consumed_power = consumed_power
  end

  def set converter
    @csms.push(converter)
  end

  def needed_power
    np = @csms.map{|x| x.total_consumed_power}.max 
    np = np.nil? ? 0 : np

    return [np , @consumed_power].map{|x| BigDecimal(x,3)}.reduce(:+).to_f
    #return np
  end

  def pipe_loss
    return @consumed_power
  end
  def childs
    @csms
  end
end
