require 'rubyvis'


def get_files(path)
  h={}
  Dir.glob("#{path}/*").each {|e|
    next if File.expand_path(e)=~/pkg|web|vendor|doc|~/
    pa=File.expand_path(e) 
    if File.stat(pa).directory?
      h[File.basename(pa)]=get_files(pa)
    else
      puts File.stat(pa).size
      h[File.basename(pa)]=File.stat(pa).size
    end
  }
  h
end

files=get_files(File.expand_path(File.dirname(__FILE__)))


vis = Rubyvis::Panel.new do
  width 200
  height 1500 
  left 40 
  right 160 
  top 10 
  bottom 10 
  layout_cluster do
    nodes pv.dom(files).root("rubyvis").sort(lambda {|a,b| a.node_name<=>b.node_name}).nodes
    group 0.2
    orient "left"

    link.line  do
      stroke_style "#ccc"
      line_width 1
      antialias false
    end

    node.dot do 
      fill_style {|n| n.first_child ? "#aec7e8" : "#ff7f0e"}
    end
    
    node_label.label
  end
end

vis.render
#puts vis.to_svg
#
