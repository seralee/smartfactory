require './generator'
require './distributor'
require './consumer'
require 'rubyvis'

#suppliers = [3,5,7].map{|x| Supplier.new x}

#assumed just one supplier case


cnt1 = Distributor.new 0.2 

[[4,0.1],[3,0.1],[2,0.1]].map{|x| Consumer.new x[0],x[1]}.each do |c|
  cnt1.set(c)
  c.set(cnt1)
end


cnt2 = Distributor.new 0.3

[[5,0.1],[6,0.1],[5.5,0.1]].map{|x| Consumer.new x[0],x[1]}.each do |c|
  cnt2.set(c)
  c.set(cnt2)
end

cnt3 = Distributor.new 0.2

[[4,0.1],[3,0.1],[4.5,0.4],[4.7,0.1]].map{|x| Consumer.new x[0],x[1]}.each do |c|
  cnt3.set(c)
  c.set(cnt3)
end


gen = Generator.new 

[cnt1,cnt2,cnt3].each do |x|
  gen.set(x)
end


def make_name c
  if c.kind_of? Generator
    "#{c.class.name[0]} : #{c.needed_power}"
  else
    "#{c.class.name[0]} : #{c.needed_power}"
  end
end

def gen_relation(p)
  h = {}
  p.childs.each do |c|
    if c.kind_of? Consumer
      h[make_name(c)] = c.needed_power
    else
      h[make_name(c)] = gen_relation(c)
    end
  end
  h
end


nodes = gen_relation(gen)


vis = Rubyvis::Panel.new do
  width 200
  height 800 
  left 40 
  right 160 
  top 10 
  bottom 10 
  layout_cluster do
    nodes pv.dom(nodes).root(make_name(gen)).sort(lambda {|a,b| a.node_name<=>b.node_name}).nodes
    group 0.2
    orient "left"

    link.line  do
      stroke_style "#ccc"
      line_width 1
      antialias false
    end

    node.dot do 
      fill_style {|n| n.first_child ? "#aec7e8" : "#ff7f0e"}
    end
    
    node_label.label
  end
end

vis.render
puts vis.to_svg
