require "bigdecimal"
class Generator
  def initialize 
    @cvts = []
  end

  def set cvt
    @cvts.push(cvt)
    @needed_power = @cvts.map{|x| [x.needed_power, x.pipe_loss].map{|y| BigDecimal(y,3)}.reduce(:+).to_f}.max
  end

  def power
    return @needed_power
  end

  def childs
    @cvts
  end
  def needed_power
    @needed_power
  end
end
