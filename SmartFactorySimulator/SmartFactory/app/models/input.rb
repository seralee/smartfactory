class Input < ActiveRecord::Base
  private
    def input_params
          params.require(:input).permit(:fixed_flow)
    end
end
