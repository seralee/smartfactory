class NewComsController < ApplicationController
  before_action :set_new_com, only: [:show, :edit, :update, :destroy]

  # GET /new_coms
  # GET /new_coms.json
  def index
    @new_coms = NewCom.all
  end

  # GET /new_coms/1
  # GET /new_coms/1.json
  def show
  end

  # GET /new_coms/new
  def new
    @new_com = NewCom.new
  end

  # GET /new_coms/1/edit
  def edit
  end

  # POST /new_coms
  # POST /new_coms.json
  def create
    @new_com = NewCom.new(new_com_params)

    respond_to do |format|
      if @new_com.save
        format.html { redirect_to @new_com, notice: 'New com was successfully created.' }
        format.json { render :show, status: :created, location: @new_com }
      else
        format.html { render :new }
        format.json { render json: @new_com.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /new_coms/1
  # PATCH/PUT /new_coms/1.json
  def update
    respond_to do |format|
      if @new_com.update(new_com_params)
        format.html { redirect_to @new_com, notice: 'New com was successfully updated.' }
        format.json { render :show, status: :ok, location: @new_com }
      else
        format.html { render :edit }
        format.json { render json: @new_com.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /new_coms/1
  # DELETE /new_coms/1.json
  def destroy
    @new_com.destroy
    respond_to do |format|
      format.html { redirect_to new_coms_url, notice: 'New com was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_new_com
      @new_com = NewCom.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def new_com_params
      params.require(:new_com).permit(:level, :portion, :power, :max_flow)
    end
end
