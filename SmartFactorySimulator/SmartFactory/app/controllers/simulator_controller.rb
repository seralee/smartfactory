class SimulatorController < ApplicationController
  def title

  end
  def floating f
    return "%.1f" % f

  end
  def input_volume_data
    #set file
    s = File.new("text","rb")

    @input = Input.first

    #set value 
    @fixed_vol = @input.fixed_flow
    @fix_used = @input.fixed_run
    gon.variable_num = @input.variable_num
    gon.fixed_num = @input.fixed_num
    @per_interval = 1 / 60.to_f / 4.to_f
    @idle_used = @input.fixed_idle
    gon.file_name = @file_name

    #init value
    @total_consumed = 0
    @categories = []
    sums = []
    result = []
    f_result = []

    var_max = @input.var_max
    var_min = @input.var_min
    fixed_flow = @input.fixed_flow


    #read data
    21.upto(6049) do |line|
      sum = 0
      l = s.gets
      ll = l.split(" ") 
      sum = ll[1].to_f
      time = ll[0].to_i

      running_compressor = get_running_processor sum, var_max, var_min,fixed_flow
      if( sum > (@fixed_vol * running_compressor))
        result.push([time, sum])
        f_result.push([time , @fixed_vol * running_compressor ])
      end
    end
    s.close
    render :json => result.to_json
  end


  def index
    @input = Input.first

  end

  def read_data
    s = File.new("text","rb")
    rt = []
    s.each_line do |l|
      ll = l.split(" ")
      rt.push [ll[0].to_i,ll[1].to_f]
    end
    s.close
    return rt
  end

  def get_running_processor sum ,var_max, var_low, fixed_flow

    n_rc = ((sum - var_max) / fixed_flow).ceil
    if(sum - (n_rc * var_max) < var_low)
      n_rc -= 1
    end
    return n_rc
  end

  def smart_grid_gen
    @NewComs = NewCom.all
    @portion = Array.new(@NewComs.length,0)
    v = 0
    @NewComs.reverse.each_with_index do |c,i|
       @portion[i] = c.portion - v
       v = c.portion
    end
    @portion = @portion.reverse

  end

  def comparison_gen
    @NewComs = NewCom.all
    @portion = Array.new(@NewComs.length,0)
    v = 0
    @NewComs.reverse.each_with_index do |c,i|
      @portion[i] = c.portion - v
      v = c.portion
    end
    @portion = @portion.reverse

  end
  def smart_grid_result
    data = read_data
    newcoms = NewCom.all
    cum = 0

    rportion = []
    portion = []
    if(params["portion"])
      portion = params["portion"].reverse
      newcoms.reverse.each_with_index do |c,i|
        cum += portion[i].to_i
        rportion[i] = cum
        c.portion = cum
        c.save!
      end
      portion = params["portion"].map{|x| x.to_i}
    end
    rportion = rportion.reverse

    input = Input.first
    energy_cost = input.energy_cost #KRW/kWh
    loss = input.losses / 100.0#%
    result = {}
    interval = 1 / 60.to_f / 4.to_f
    result["power_consumption"] = 0
    result["volume"] = {}
    result["power_per_portion"] = {name:"SmartGrid",data:Array.new(4,0)}
    result["power_per_portion_categories"] = [] 

    @comps = []
    newcoms.each_with_index do |c,i|
      @comps.push([c.level,c.portion,c.power,c.max_flow,0])
      if(portion)
        result["power_per_portion_categories"].push ("#{ c.level.to_s} Bar <br> #{portion[i].to_s}%")
      end
    end

    @opts = [ 0.9, 0.7, 0.45 ] # high, low, min
    @non_opt_aero = 1.10 
    @interval = 15
    @per = 1/3600.0

    @total_flow = 0
    @total=Array.new(4,0)
    @total_volume=Array.new(4,0)

    result["level_power_consumption"] = {}
    result["level_power_consumption"]["categories"] = ["SmartGrid","Existing"]

    #read data
    data.each do |d|
      sum = d[1]
      @total_flow += sum
      po = @comps.map{|x| calculate_power sum, x}
      vo = @comps.map{|x| sum * x[1] / 100.0}
      po.each_with_index do |e,i|
        result["power_consumption"] +=  e * interval
        @total[i] += e * interval
        @total_volume[i] += vo[i] * interval
      end
    end

    result["volume"]["categories"] = @comps.map{|x| "Var-"+x[0].to_s}
    result["volume"]["series"] = [{name: "Volume", data:@total_volume.map{|x| x.to_i}}]
    result["level_power_consumption"]["series"] = []
    colors = ["#c9fdcc","#01ff3d","#349c3d","#003504"]
    @total.each_with_index do |t,i|
      result["level_power_consumption"]["series"].push({name: "#{(@comps[i][0]).to_s} Bar", data:[t,0],color:colors[i]})
    end
    result["level_power_consumption"]["series"].reverse!
    result["variable_num"] = @comps.map{|x| x[4]}
    result["power_consumption"] *= 1 / (1-loss)
    result["power_consumption_year"] = (result["power_consumption"] / 25.1 * 24 * 365 /1000).to_i
    result["energy_bill"] = (result["power_consumption_year"] * energy_cost  / 1000).to_i
    result["real_portion"] = rportion[1..-1]

    result["power_consumption_per_volume_year"] = floating (result["power_consumption"] * energy_cost / (@total_flow / 4.0))

    if(params["portion"])
      4.times do |i|
        (i+1).times do |ii|
          result["power_per_portion"][:data][i] += @total[ii] * portion[i] / rportion[ii].to_f 
        end
      end
    end


    result["specific_power"] = floating (result["power_consumption"] / (@total_flow * interval ))

    result["power_consumption"] = result["power_consumption"].to_i 
    render :json => result.to_json

  end

  def recur_graph list
    len = list.length
    if(len >= 2)
      return { "children" =>  [{ "type" => list[0], "value" => 10, "odd" => 0}, (recur_graph list[2..-1] ), {"type" => list[1], "value" => 10, "odd" => 0}] }
    elsif len == 0
      return {}
    elsif len == 1
      return {"type" => list[0] , "value" =>  10 , "odd" => 1}
    end

  end

  def status_graph state_list
    color = {"Run" => "#05afed","Stop" => "#bfbfbf","Idle" =>"#ffc22b","Var" => "#006813"}
    status = state_list.map{|x|  
      if x == -1
        color["Stop"]
      elsif x > 0
        color["Idle"]
      else
        color["Run"]
      end
    } + [color["Var"]]

    treeData = {}
    treeData["parent"] = "null"
    treeData["children"] = [{"children" => [( recur_graph status) ] }]
    return treeData
  end

  def original_result
    data = read_data

    input = Input.first
    fixed_num = input.fixed_num
    variable_num = input.variable_num
    fixed_flow = input.fixed_flow
    var_max = input.var_max
    var_min = input.var_min
    fixed_run = input.fixed_run
    fixed_idle = input.fixed_idle
    var_power = input.var_power
    interval = 1 / 60.to_f / 4.to_f
    energy_cost = input.energy_cost
    itv_status = 30
    portion = []

    if(params["portion"])
      portion = params["portion"].map{|x| x.to_i}
    end

    @state = Array.new(fixed_num,-1)
    result = {}
    result["mode"] = {}
    result["volume"] = Array.new(fixed_num+variable_num,0)
    result["power_consumption"] = 0
    result["energy_bill"] = 0
    result["specific_power"] = 0
    result["fixed_num"] = fixed_num
    result["variable_num"] = variable_num
    result["level_power_consumption"] = {}
    result["original_status"] = []
    result["power_per_portion" ] = {name: 'Existing', data:[]}
    result["power_per_time"] = [{name:"Total",data:[]},{name:"Fixed",data:[]}]
    @total_cnt = 0
    @idle_cnt = 0
    @total_flow = 0
    result["mode"]["series"] = []
    result["mode"]["categories"] = [*1..(fixed_num+variable_num)]
    color = ["#05afed","#ffc22b","#bfbfbf","#006813"]
    result["volume"] = {}
    result["volume"]["series"] = [{name: "Volume",data:Array.new(fixed_num + variable_num,0)}]
    result["volume"]["categories"] = [*1..(fixed_num+variable_num)]

    result["level_power_consumption"]["categories"] = ["power consumption"]

    #set mode
    ["Run","Idle","Stop","Var"].each_with_index do |s,i|
      result["mode"]["series"].push({name: s,color:color[i],data: Array.new(fixed_num+variable_num,0)})
    end


    #infer data
    data.each_with_index do |d,i|

      time = d[0]
      sum = d[1]
      @total_flow += sum 
      f = get_running_processor sum, var_max, var_min, fixed_flow

      power_consumption = 0

      fixed_num.times do |fi|
        if(fi < f)
          @state[fi] = 0
          result["mode"]["series"][0][:data][fi] += 1
          result["volume"]["series"][0][:data][fi] += fixed_flow
          power_consumption +=  fixed_run * interval 
        else
          if(@state[fi] != -1) 
            @state[fi] = @state[fi] + 1
            if(@state[fi] == 40)
              @state[fi] = -1
              result["mode"]["series"][2][:data][fi] += 1
            else
              result["mode"]["series"][1][:data][fi] += 1
              power_consumption +=  fixed_idle  * interval
            end
          else
            result["mode"]["series"][2][:data][fi] += 1
          end
        end
        result["power_per_time"][0][:data].push [time, sum] 
        result["power_per_time"][1][:data].push [time, f * fixed_flow] 
      end

      @total_cnt += 11
      variable_num.times do |v|
      result["mode"]["series"][3][:data][v+fixed_num] += 1

      var_vol = sum - f * fixed_flow
      power_consumption +=  var_vol * var_power  * interval
      result["volume"]["series"][0][:data][v+fixed_num] += var_vol 
      end

      result["power_consumption"] += power_consumption

      idles = @state.count{|x| x > 0}
      @idle_cnt += idles

      if(i % itv_status == 0)
        result["original_status"].push status_graph(@state)
      end
    end
     p @total_flow * 24 / 25.1  * 365
    result["mode"]["series"] = result["mode"]["series"].reverse
    result["energy_bill"] = ((result["power_consumption"] * energy_cost / 25.1 * 24 * 365) / 1000000).to_i
    result["specific_energy"] = floating (result["power_consumption"] / (@total_flow * interval ))
    result["power_consumption_year"] = ((result["power_consumption"] / 25.1 * 24 * 365) / 1000).to_i
    result["power_consumption_per_volume_year"] = floating( result["power_consumption"] * energy_cost /  (@total_flow/4.0))
    result["power_consumption"] = result["power_consumption"].to_i
    result["level_power_consumption"]["series"] = [{name:"7.5 Bar",data: [0,result["power_consumption"]],color:"#434348"}]

    portion.each do |p|
      result["power_per_portion"][:data].push result["power_consumption"] * p / 100.0
    end

    render :json => result.to_json
  end


  def calculate_power sum , com
    pr = com[0]
    vol = com[1]
    c_po = com[2]
    f_max = com[3]
    @opts = [ 0.9, 0.7, 0.45 ] # high, low, min
    @non_opt_aero = 1.10 
    consumed_vol = sum * vol / 100.to_f
    #puts consumed_vol
    qty = (consumed_vol / f_max).ceil
    if(qty + 1 > com[4])
      com[4] = qty + 1
    end
    round = (consumed_vol/qty)
    high = f_max * @opts[0]
    low = f_max * @opts[1]

    ret = 0 
    if( round <= high and round >= low)
      ret =  consumed_vol * c_po
    else
      ret = consumed_vol * c_po * @non_opt_aero
    end

    return ret
  end

  def get_power_of_variable v
    @v_to_p = { 1167.0*alpha * ftm => 209.0, 1041.0*alpha* ftm => 182.2, 967.0*alpha* ftm => 167.6, 817.0*alpha* ftm => 140.1, 587.0*alpha* ftm => 102.0, 270.0*alpha* ftm => 54.1}
    size = @v_to_p.size
    keys = @v_to_p.keys.reverse
    range = []

    #find the similar range
    keys.each_with_index do |e,i|
      if(e > v)
        if(i == (size-1))
          range = [i]
          break
        elsif i == 0
          range = [0]
          break
        else
          range = [i,i+1]
          break
        end
      elsif (e == v)
        return e / 100 * v
      else
        range = [i]
        #e < v
        next
      end
    end
    if(range.size == 1)
      v1 = keys[range[0]]
      p1 = @v_to_p[v1]

      #over the range
      if(range[0] == 0)
        v2 = keys[1]
        p2 = @v_to_p[v2]
        pv2 = p2/v2
        pv1 = p1/v1
        return (pv2 + (pv1 - pv2) / (v2 - v1) * (v2 - v)) * v
      else
        v2 = keys[range[0] - 1]
        p2 = @v_to_p[v2]
        pv2 = p2/v2
        pv1 = p1/v1
        return (pv2 + (pv1 - pv2) / (v2 - v1) * (v - v1)) * v
      end


    else
      v1 = keys[range[0]]
      v2 = keys[range[1]]
      p1 = @v_to_p[v1]
      p2 = @v_to_p[v2]
      pv2 = p2/v2
      pv1 = p1/v1
      if(p1 > p2)
        return (pv2 + (pv1 - pv2)/(v2 - v1)*(v2 - v)) * v
      else
        return (pv1 + (v - v1) * (pv2 - pv1) / (v2 - v1)) * v
      end
    end
  end

  def get_chart
    mode = params[:mode]
    if mode == "original"

    else
      # smart grid 
    end
  end

end
