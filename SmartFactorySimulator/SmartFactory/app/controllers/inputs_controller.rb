class InputsController < ApplicationController
  before_action :set_input, only: [:show, :edit, :update, :destroy]

  # GET /inputs
  # GET /inputs.json
  def index
    @inputs = Input.all
    @new_coms = NewCom.all
    @NewComs = NewCom.all
    @portion = []
    v = 0
    @NewComs.reverse.each do |c|
       @portion.push c.portion - v
       v = c.portion
    end
    @portion = @portion.reverse
    @machines = Machine.all
    @input = Input.first
  end

  def settings
    @input = Input.first
    @NewComs = NewCom.all
    @portion = []
    v = 0
    @NewComs.reverse.each do |c|
      @portion.push c.portion - v
      v = c.portion
    end
    @portion = @portion.reverse
    render "settings",:layout => false 
    
  end

  # GET /inputs/1
  # GET /inputs/1.json
  def show
  end

  # GET /inputs/new
  def new
    @input = Input.new
  end

  # GET /inputs/1/edit
  def edit
  end

  # POST /inputs
  # POST /inputs.json
  def create
    @input = Input.new(input_params)

    respond_to do |format|
      if @input.save
        format.html { redirect_to @input, notice: 'Input was successfully created.' }
        format.json { render :show, status: :created, location: @input }
      else
        format.html { render :new }
        format.json { render json: @input.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /inputs/1
  # PATCH/PUT /inputs/1.json
  def update
    if params["com"]
      keys = params["com"].keys.sort.reverse
      newcoms = NewCom.all.reverse
      v = 0
      keys.each_with_index do |k,i|
        com = params["com"][k]
        ncom = newcoms[i]
        ncom.level = com["level"]
        v = v + com["portion"].to_i
        ncom.portion = v
        ncom.power = com["power"]
        ncom.save!
      end
    end
    respond_to do |format|
      if @input.update(input_params)
        format.html { redirect_to action: "index", notice: 'Inputs are uploaded successfully'}
        format.json { render :show, status: :ok, location: @input }
      else
        format.html { render :edit }
        format.json { render json: @input.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /inputs/1
  # DELETE /inputs/1.json
  def destroy
    @input.destroy
    respond_to do |format|
      format.html { redirect_to inputs_url, notice: 'Input was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_input
      @input = Input.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def input_params
      params.require(:input).permit(:energy_cost, :losses, :fixed_num, :variable_num, :fixed_power, :fixed_run, :fixed_idle, :var_min, :var_max, :var_power,:fixed_flow)
    end
end
