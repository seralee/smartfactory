json.array!(@machines) do |machine|
  json.extract! machine, :id, :kWh, :price
  json.url machine_url(machine, format: :json)
end
