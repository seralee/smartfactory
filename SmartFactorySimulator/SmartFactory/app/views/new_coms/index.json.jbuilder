json.array!(@new_coms) do |new_com|
  json.extract! new_com, :id, :level, :portion, :power, :max_flow
  json.url new_com_url(new_com, format: :json)
end
