json.array!(@inputs) do |input|
  json.extract! input, :id, :energy_cost, :losses, :fixed_num, :variable_num, :fixed_flow, :fixed_run, :fixed_idle, :var_min, :var_max, :var_power
  json.url input_url(input, format: :json)
end
