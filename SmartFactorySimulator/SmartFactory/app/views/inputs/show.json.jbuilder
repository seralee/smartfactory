json.extract! @input, :id, :energy_cost, :losses, :fixed_num, :variable_num, :fixed_flow, :fixed_run, :fixed_idle, :var_min, :var_max, :var_power, :created_at, :updated_at
