# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://coffeescript.org/#
createInputChart = () -> 
 $.getJSON '/input_volume_data', (data) ->
    # Create the chart
    $('#volume_input_data').highcharts 'StockChart',
      exporting: enabled: false
      credits: enabled: false
      buttonTheme: visibility: 'hidden'
      labelStyle: visibility: 'hidden'
      scrollbar: visibiliry: 'hidden'
      yAxis: min: 0
      rangeSelector: enabled: false
      navigator: enabled: false
      series: [ {
        name: 'input'
        data: data
        tooltip: valueDecimals: 2
      } ]
      $(".highcharts-scrollbar").hide()
    return
  return
createChart = (series) ->
  $('#power_chart').highcharts 'StockChart',
    exporting: enabled: false
    credits: enabled: false
    buttonTheme: visibility: 'hidden'
    labelStyle: visibility: 'hidden'
    scrollbar: visibiliry: 'hidden'
    rangeSelector: enabled: false
    navigator: enabled: false
    yAxis:
      min: 0
      labels: formatter: ->
        (if @value > 0 then ' + ' else '') + @value + 'm<sup>3</sup>'
      plotLines: [ {
        value: 0
        width: 2
        color: 'silver'
      } ]
    tooltip:
      pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}<br/>'
      valueDecimals: 2
    series: series 
  $(".highcharts-scrollbar").hide()
  return

createModeChart = (categories, series) ->
  $('#mode_chart').highcharts
    chart: type: 'column'
    exporting: enabled: false
    credits: enabled: false
    title: text: 'Operation Mode'
    xAxis: categories: categories
    yAxis: 
      min: 0
      title: text:"%"
    tooltip:
      pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>'
      shared: true
    plotOptions: column: stacking: 'percent'
    series: series
    buttonTheme: 
      visibility: 'hidden'
    labelStyle: 
      visibility: 'hidden'
    rangeSelector:
      enabled:false
  return

createVolumeChart = (categories, series) ->
  $("#volume_chart").html ""
  $('#volume_chart').highcharts
    chart: type: 'column'
    credits: enabled: false
    exporting: enabled: false
    title: text: 'Volume Chart'
    xAxis:
      categories: categories
      crosshair: true
    yAxis:
      min: 0
      title: text: 'm<sup>3</sup>'
    tooltip:
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>'
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y:.1f} m<sup>3</sup></b></td></tr>'
      footerFormat: '</table>'
      shared: true
      useHTML: true
    plotOptions: column:
      pointPadding: 0.2
      borderWidth: 0
    buttonTheme: 
      visibility: 'hidden'
    labelStyle: 
      visibility: 'hidden'
    rangeSelector:
      enabled:false
    series: series
  return


$(document).on 'page:change ready', (event) ->
  createInputChart()

$(document).on 'ready page:load', (event) ->

  createModeChart([],[{name:"Var",color:"#006813",data:[]},{name:"Stop",color:"#bfbfbf",data:[]},{name:"Idle",color:"#ffc22b",data:[]},{name:"Run",color:"#05afed",data:[]}])
  createVolumeChart([],[{name:"Volume",data:[]}])

  $("#simulation").click ->
    $.ajax
      type: 'GET'
      url: '/original_total_result'
      success: (data)->
        $("#power_consumption").html(data.power_consumption)
        $("#specific_energy").html(data.specific_energy)
        $("#energy_bill").html(data.energy_bill)
        createModeChart(data.mode.categories,data.mode.series)
        createVolumeChart(data.volume.categories,data.volume.series)
        createChart(data.power_per_time)

  $("#smart_simulation").click ->
    $.ajax
      type: 'GET'
      url: '/smart_grid_result'
      data: {portion: $('.portion').map(->$(this).val()).get()}
      success: (data)->
        $("#smart_loaded").html("1")
        $("#power_consumption").html(data.power_consumption)
        $("#specific_energy").html(data.specific_power)
        $("#energy_bill").html(data.energy_bill)
        createVolumeChart(data.volume.categories,data.volume.series)

   $("#show_details").click ->
     $("#config_details").show()
     $.ajax
       type: 'GET'
       url: '/setting'
       cache: false
       success: (html) ->
         $("#config_details").html html

   $("#hide_details").click ->
     $("#config_details").html ""
     $("#config_details").hide()


  $('.dropdown-item').click ->
    $('.image').hide()
    chartType = $(this).data('type')
    chartImg = $(this).data('path')
    $('#result_type').html '<h3> ' + chartType + ' </h3>'
    $(document.getElementById(chartImg)).show()

    return


