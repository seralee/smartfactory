createCompareChart = (categories, series) ->
  $('#power_level2').highcharts
    exporting: enabled: false
    credits: enabled: false
    chart: type: 'column'
    title: text: ''
    xAxis: categories: categories
    yAxis:
      min: 0
      title: text: 'Consumped Power (kWh)'
      stackLabels:
        enabled: false
    tooltip: pointFormat: '{series.name}: {point.y}<br/>Total: {point.stackTotal}'
    plotOptions: column: stacking: 'normal'
    series: series
  return

createBarChart = (id,categories, series) ->
  $(id).highcharts
   chart: type: 'bar'
   title: text: ''
   xAxis: categories: categories 
   exporting: enabled: false
   credits: enabled: false
   yAxis:
     min: 0
     max : 40000
   legend: reversed: true
   plotOptions: series: stacking: 'normal'
   series: series 
  return  

createBarCompareChart = (categories, series) -> 
  $('#power_level').highcharts
    exporting: enabled: false
    credits: enabled: false
    title: text: ''
    chart: type: 'column'
    xAxis:
      categories: categories 
      crosshair: true
    yAxis:
      min: 0
      title: text: 'Consumped power (kWh)'
    tooltip:
      headerFormat: '<span style="font-size:10px">{point.key}</span><table>'
      pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' + '<td style="padding:0"><b>{point.y:.1f} kWh</b></td></tr>'
      footerFormat: '</table>'
      shared: true
      useHTML: true
    plotOptions: column:
      pointPadding: 0.2
      borderWidth: 0
    series: series
  return


createPowerChart = (data) ->
  $('#gap_chart').highcharts 'StockChart',
      chart: type: 'arearange'
      title: text: 'Power consumption'
      tooltip: valueSuffix: 'kWh'
      buttonTheme: 
        visibility: 'hidden'
      labelStyle: 
        visibility: 'hidden'
      credits: enabled: false
      exporting: enabled: false
      navigator: enabled: false
      scrollbar: visibility: 'hidden'
      rangeSelector: enabled: false
      series: [ {
        name: 'Power Consumption'
        data: data
      } ]
  $(".highcharts-scrollbar").hide()

result_load = (portion) ->
  $.ajax
    type: 'GET'
    url: '/original_total_result'
    cache: false
    data: portion 
    success: (data)->
      createBarChart("#ori_power_details",data.level_power_consumption.categories,data.level_power_consumption.series)
      $("#ori_fixed_num").html(data.fixed_num)
      $("#ori_variable_num").html(data.variable_num)
      $("#ori_power_consumption").html(data.power_consumption)
      $("#ori_power_consumption_year").html(data.power_consumption_year)
      $("#ori_energy_bill").html(data.energy_bill)
      $("#ori_volume_power").html(data.power_consumption_per_volume_year)
      $("#ori_specific_power").html(data.specific_energy)
      power_time = data.power_consumption_time
      $.ajax
        type: 'GET'
        url: '/smart_grid_result'
        cache: false
        data: portion
        success: (sdata)->
          createBarChart("#smart_power_details",sdata.level_power_consumption.categories,sdata.level_power_consumption.series)
          $("#var2_num").html(sdata.variable_num[0])
          $("#var4_num").html(sdata.variable_num[1])
          $("#var6_num").html(sdata.variable_num[2])
          $("#var8_num").html(sdata.variable_num[3])
          $("#smart_power_consumption").html(sdata.power_consumption)
          $("#smart_power_consumption_year").html(sdata.power_consumption_year)
          $("#smart_energy_bill").html(sdata.energy_bill)
          $("#smart_volume_power").html(sdata.power_consumption_per_volume_year)
          $("#smart_specific_power").html(sdata.specific_power)
          $("#Saved").html(Math.round((data.specific_energy - sdata.specific_power)/data.specific_energy * 100.0)) 
          createBarCompareChart(sdata.power_per_portion_categories, [sdata.power_per_portion,data.power_per_portion])
          createCompareChart(sdata.level_power_consumption.categories,data.level_power_consumption.series.concat(sdata.level_power_consumption.series))

$(document).on 'ready page:load', (event) ->
  power_time = []
  result_load({portion: $('.portion').map(->$(this).val()).get()})

  $("#compare_simulate").click ->
    result_load({portion: $('.portion').map(->$(this).val()).get()})

