$(document).ready ->
  ###
  $('#container_mode').highcharts
    chart: type: 'column'
    title: text: 'Mode'
    xAxis: categories: gon.category 
    yAxis:
      min: 0
      title: text: 'Total mode count'
    tooltip:
      pointFormat: '<span style="color:{series.color}">{series.name}</span>: <b>{point.y}</b> ({point.percentage:.0f}%)<br/>'
      shared: true
    plotOptions: column: stacking: 'percent'
    series: [
      {
        name: 'running'
        data: gon.running 
      }
      {
        name: 'idle'
        data: gon.idle 
      }
      {
        name: 'shut down'
        data:  gon.down
      }
    ]
  return
  ###

# ---
# generated by js2coffee 2.2.0
