require "simple-spreadsheet"

file_name = 'data.csv'
s = SimpleSpreadsheet::Workbook.read(file_name, ".csvx")
s.selected_sheet = s.sheets.first
sums = []
interval = 1
sum = 0

result = []
s.first_row.upto(s.last_row) do |line|
  sum = 0
  (2..4).each do |i|
    sum = sum + s.cell(line,i).split(",")[0].to_f
  end
  time = s.cell(line,1).split(" ")
  time[0] = "2016/"+time[0].split("/").shift(2).join("/")

  puts "time #{Time.parse(time.join(" "))} sum #{sum}"
  sums.push(sum)
  result.push([Time.parse(time.join(" ")).to_i * 1000 , sum])
end
#lines = s.first_row.upto(s.last_row)
puts "file name : #{file_name}"


alpha = 0.97

@v_to_p = { 1167.0*alpha => 209.0, 1041.0*alpha => 182.2, 967.0*alpha => 167.6, 817.0*alpha => 140.1, 587.0*alpha => 102.0, 270.0*alpha => 54.1}
@v_to_p.keys.map{|x| @v_to_p[x] = @v_to_p[x] / alpha }
@fixed = [170.0,(1010*alpha)]

def get_power_of_variable v
  size = @v_to_p.size
  keys = @v_to_p.keys.reverse
  range = []

  #find the similar range
  keys.each_with_index do |e,i|
    if(e > v)
      if(e == (size-1))
        range = [i]
        break
      elsif i == 0
        range = [0]
        break
      else
        range = [i,i+1]
        break
      end
    elsif (e == v)
      return e / 100 * v
    else
      #e < v
      next
    end
  end
  if(range.size == 1)
    v1 = keys[range[0]]
    p1 = @v_to_p[v1]

    #over the range
    if(range[0] == 0)
      v2 = keys[1]
      p2 = @v_to_p[v2]
      pv2 = p2/v2
      pv1 = p1/v1
      return (pv2 + (pv1 - pv2) / (v2 - v1) * (v2 - v)) * v
    else
      v2 = keys[range[0] - 1]
      p2 = @v_to_p[v2]
      pv2 = p2/v2
      pv1 = p1/v1
      return (pv2 + (pv1 - pv2) / (v2 - v1) * (v - v1)) * v
    end


  else
    v1 = keys[range[0]]
    v2 = keys[range[1]]
    p1 = @v_to_p[v1]
    p2 = @v_to_p[v2]
    pv2 = p2/v2
    pv1 = p1/v1
    if(p1 > p2)
      return (pv2 + (pv1 - pv2)/(v2 - v1)*(v2 - v)) * v
    else
      return (pv1 + (v - v1) * (pv2 - pv1) / (v2 - v1)) * v
    end
  end
end

results = []
sums.each do |s|
  f = (s/@fixed[1]).to_i
  v = s - f*@fixed[1]
  vf = get_power_of_variable v
#  puts "run fixed : #{f} , variable #{v} ( #{vf} ), total : #{vf + 170 * f}"
end
p result
