require "simple-spreadsheet"

s = SimpleSpreadsheet::Workbook.read("new_data.xls")
s.selected_sheet = s.sheets.first

energy_cost = 103 #KRW/kWh
loss = 10 #%
@comps = []
@comps.push([2, 20,1.64,100.0]) #pressure, volumn, consumed power, flow_max
@comps.push([4, 20,1.67, 95.0])
@comps.push([6, 40,0.94, 90.0])
@comps.push([8, 20,0.66, 70.0])
@opts = [ 0.9, 0.7, 0.45 ] # high, low, min
@non_opt_aero = 1.10 
@interval = 15
@per = 1/3600.0

@total = [0,0,0,0]
a = File.new("test1","rb")
b = File.new("test2","w")

def calculate_power sum , pr, vol, c_po, f_max
  consumed_vol = sum * vol / 100.to_f
  #puts consumed_vol
  qty = (consumed_vol / f_max).ceil
  round = (consumed_vol/qty)
  high = f_max * @opts[0]
  low = f_max * @opts[1]

  ret = 0 
  if( round <= high and round >= low)
    ret =  consumed_vol * c_po
  else
    ret = consumed_vol * c_po * @non_opt_aero
  end

  return ret
end



#read data
22.upto(s.last_row) do |line|
  sum = 0
  ss = a.readline.strip
  if((ss.to_f - s.cell(line,16).to_f).abs > 1)
    b.puts "#{line} #{ss} #{s.cell(line,16)}"
  end
  (2..4).each do |i|
    sum = sum + s.cell(line,i).to_f
  end
  time = s.cell(line,1).split(" ")
  time[0] = "2016/"+time[0].split("/").shift(2).join("/")
  po = @comps.map{|x| calculate_power sum, x[0], x[1], x[2], x[3]}
  po.each_with_index do |e,i|
    @total[i] += e
  end

  #puts "time #{Time.parse(time.join(" "))} sum #{sum}"
  
end
b.close
p @total.map{|x| x * @interval * @per}
p @total.map{|x| x * @interval * @per}.sum * energy_cost

