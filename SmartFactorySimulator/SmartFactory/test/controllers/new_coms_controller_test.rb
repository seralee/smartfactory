require 'test_helper'

class NewComsControllerTest < ActionController::TestCase
  setup do
    @new_com = new_coms(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:new_coms)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create new_com" do
    assert_difference('NewCom.count') do
      post :create, new_com: { level: @new_com.level, max_flow: @new_com.max_flow, portion: @new_com.portion, power: @new_com.power }
    end

    assert_redirected_to new_com_path(assigns(:new_com))
  end

  test "should show new_com" do
    get :show, id: @new_com
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @new_com
    assert_response :success
  end

  test "should update new_com" do
    patch :update, id: @new_com, new_com: { level: @new_com.level, max_flow: @new_com.max_flow, portion: @new_com.portion, power: @new_com.power }
    assert_redirected_to new_com_path(assigns(:new_com))
  end

  test "should destroy new_com" do
    assert_difference('NewCom.count', -1) do
      delete :destroy, id: @new_com
    end

    assert_redirected_to new_coms_path
  end
end
