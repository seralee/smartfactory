class CreateInputs < ActiveRecord::Migration
  def change
    create_table :inputs do |t|
      t.integer :energy_cost
      t.integer :losses
      t.integer :fixed_num
      t.integer :variable_num
      t.float :fixed_flow
      t.float :fixed_run
      t.float :fixed_idle
      t.float :var_min
      t.float :var_max
      t.float :var_power

      t.timestamps null: false
    end
  end
end
