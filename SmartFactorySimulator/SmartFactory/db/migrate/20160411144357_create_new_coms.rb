class CreateNewComs < ActiveRecord::Migration
  def change
    create_table :new_coms do |t|
      t.integer :level
      t.integer :portion
      t.float :power
      t.float :max_flow

      t.timestamps null: false
    end
  end
end
