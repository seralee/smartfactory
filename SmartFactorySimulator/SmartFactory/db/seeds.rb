# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
#
Input.create(energy_cost: 103,losses: 8,fixed_num:11,variable_num: 1,fixed_flow: 25.6,fixed_run: 171,fixed_idle: 53,var_min: 7.9,var_max: 25.8,var_power: 7.6)
NewCom.create(level:1,portion:100,power:1.64,max_flow:100.0)
NewCom.create(level:3,portion:80,power:1.67,max_flow:95.0)
NewCom.create(level:5,portion:60,power:0.94,max_flow:90.0)
NewCom.create(level:7,portion:40,power:0.66,max_flow:70.0)

Machine.create(kWh:75,price:60000000)
Machine.create(kWh:150,price:90000000)
Machine.create(kWh:350,price:200000000)
