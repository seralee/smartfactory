var App = angular.module("App", ["ngTouch"]);

App.controller("CarCtrl", ($scope) => {
  $scope.fuelLeft = 20;
  $scope.radioVolume = 60;
});

App.directive("gauge", ($swipe) => {
  return {
    retrict: 'EA',
    scope: {
      percentage: '@'
    },
    controller: ($scope) => {
      $scope.pointerStyle = (value = 100) => `transform: rotate(${value * 1.8 - 90}deg)`;
      $scope.areaStyle = (value = 100) => `transform: rotate(${value * 1.8 - 180}deg)`;
    },
    link: function(scope, element, attrs) {
      scope.swipe = (e) => {
        $swipe.bind(element, {
          start: function(){
            console.log()
          },
          end: function(){
            
          }
          
        });
        
        console.log(element);
        
        console.log($swipe);
      }
    },
    template: `
      <div class="gauge" ng-swipe-left="swipe($event)" ng-swipe-right="swipe($event)">
        <div class="gauge-inner">
          <div class="gauge-area" style="{{areaStyle(percentage)}}"></div>
        </div>
        <div class="gauge-pointer" style="{{pointerStyle(percentage)}}"></div>
      </div>
    `
  }

});
