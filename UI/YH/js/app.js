(function() {
  var MainCtrl, ProgressCtrl, app;

  app = angular.module('circular', []);

  app.run();

  app.controller('MainCtrl', MainCtrl = (function() {
    function MainCtrl() {}

    MainCtrl.prototype.actual = 0.5;

    MainCtrl.prototype.expected = 0.5;

    MainCtrl.prototype.numbers = [
      {
        actual: 0.2,
        expected: 0.3
      }, {
        actual: 0.4,
        expected: 0.6
      }, {
        actual: 0.6,
        expected: 0.9
      }
    ];

    MainCtrl.prototype.random = function() {
      var i, len, number, ref, results;
      ref = this.numbers;
      results = [];
      for (i = 0, len = ref.length; i < len; i++) {
        number = ref[i];
        number.actual = Math.random();
        results.push(number.expected = Math.random());
      }
      return results;
    };

    return MainCtrl;

  })());

  app.controller('ProgressCtrl', ProgressCtrl = (function() {
    function ProgressCtrl() {
      this.τ = 2 * Math.PI;
      this.width = 200;
      this.height = 200;
      this.rad = (this.width / 2) - 10;
      this.offset = this.rad * 0.02;
    }

    ProgressCtrl.prototype.arc = function(radius) {
      return d3.svg.arc().outerRadius(radius).innerRadius(radius).startAngle(0);
    };

    ProgressCtrl.prototype.line = function(color, width, d) {
      return this.svg.append('path').datum({
        endAngle: 0
      }).style('stroke', color).style('stroke-width', width).attr('d', d).attr('stroke-linejoin', 'round');
    };

    ProgressCtrl.prototype.getColor = function(value) {
      if (value < 0.25) {
        return '#ff0000';
      } else if (value < 0.50) {
        return '#F7640A';
      } else {
        return '#78c000';
      }
    };

    ProgressCtrl.prototype.getFontSize = function(value) {
      var length;
      length = (Number(value) || 0).toFixed(0).length;
      if (length >= 7) {
        return '1.7em';
      }
      if (length >= 6) {
        return '2em';
      } else if (length >= 4) {
        return '2.5em';
      } else {
        return '3em';
      }
    };

    ProgressCtrl.prototype.createTween = function(arc) {
      return function(transition, newAngle) {
        return transition.attrTween('d', function(d) {
          var interpolate;
          interpolate = d3.interpolate(d.endAngle, newAngle);
          return function(t) {
            d.endAngle = interpolate(t);
            return arc(d);
          };
        });
      };
    };

    ProgressCtrl.prototype.render = function(element) {
      var circle, line1, line2;
      this.svg = d3.select(element[0]).append('svg').attr('width', this.width).attr('height', this.height).append('g').attr('transform', 'translate(' + this.width / 2 + ',' + this.height / 2 + ')');
      this.outerArc = this.arc(this.rad);
      this.innerArc = this.arc(this.rad - (this.offset + 8));
      this.outer = this.line('orange', '8px', this.outerArc);
      this.inner = this.line('#c7e596', '4px', this.innerArc);
      circle = this.svg.append('circle').attr('r', this.rad - this.offset * 2 - 15).attr('fill', '#f5f5f5');
      line1 = this.svg.append('text').style("text-anchor", "middle");
      this.number = line1.append('tspan').style('font-size', this.getFontSize(0)).style('fill', '#444').text('0');
      line1.append('tspan').style('font-size', '1.5em').text('kWh');
      return line2 = this.svg.append('text').attr('dy', '10%').style("text-anchor", "middle").style('font-size', '1em').style('fill', '#888').text('Pressure');
    };

    ProgressCtrl.prototype.update = function() {
      var actual, actualPercentage, expected;
      actual = [0, parseFloat(this.actual) || 0, 1].sort()[1];
      expected = [0, parseFloat(this.expected) || 0, 1].sort()[1];
      actualPercentage = (parseFloat(this.actual) || 0) * 100;
      this.outer.transition().duration(1000).style('stroke', this.getColor(actual)).call(this.createTween(this.outerArc), actual * this.τ);
      this.inner.transition().duration(1000).call(this.createTween(this.innerArc), expected * this.τ);
      return this.number.text(actualPercentage.toFixed(0)).style('font-size', this.getFontSize(actualPercentage));
    };

    return ProgressCtrl;

  })());

  app.directive('progress', function() {
    return {
      scope: {},
      bindToController: {
        actual: '=',
        expected: '='
      },
      controller: 'ProgressCtrl as ctrl',
      link: function(scope, element, attrs, ctrl) {
        ctrl.render(element);
        scope.$watch('ctrl.actual', function() {
          return ctrl.update();
        });
        return scope.$watch('ctrl.expected', function() {
          return ctrl.update();
        });
      }
    };
  });

}).call(this);
